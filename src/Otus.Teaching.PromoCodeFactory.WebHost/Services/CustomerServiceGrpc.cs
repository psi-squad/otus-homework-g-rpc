﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices
{
    public class CustomerServiceGrpc : Customer.CustomerBase
    {
        private readonly ILogger<CustomerServiceGrpc> _logger;
        private readonly IRepository<Core.Domain.PromoCodeManagement.Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerServiceGrpc(ILogger<CustomerServiceGrpc> logger
            , IRepository<Core.Domain.PromoCodeManagement.Customer> customerRepository
            , IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _logger.LogInformation("gRPC init");
        }

        public override async Task<CustomerShortResponseListGrpc> GetCustomers(NoParameter request, ServerCallContext context)
        {
            _logger.LogInformation("gRPC request GetCustomers has been got");

            var customerShortResponseListGrpc = new CustomerShortResponseListGrpc();
            var customers = await _customerRepository.GetAllAsync();
            foreach (Core.Domain.PromoCodeManagement.Customer customer in customers)
            {
                customerShortResponseListGrpc.CustomersShortResponse.Add(new CustomerShortResponseGrpc()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                });
            }

            return customerShortResponseListGrpc;
        }

        public override async Task<CustomerResponseGrpc> GetCustomer(CustomerId request, ServerCallContext context)
        {
            _logger.LogInformation("gRPC request GetCustomer has been got");

            CustomerResponseGrpc CustomerResponseGrpc = new CustomerResponseGrpc();
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                return null;
            else
            {
                var customerGrpc = new CustomerResponseGrpc()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                };
                if (customer.Preferences.Any())
                    customerGrpc.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponseGrpc()
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name
                    }));


                return customerGrpc;
            }
        }

        public override async Task<CustomerResponseGrpc> CreateCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            _logger.LogInformation("gRPC request CreateCustomer has been got");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            Core.Domain.PromoCodeManagement.Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerResponseGrpc() { Id = customer.Id.ToString() };

        }

        public override async Task<CustomerResponseGrpc> EditCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            _logger.LogInformation("gRPC request EditCustomer has been got");

            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Not found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            customer = CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            var customerGrpc = await GetCustomer(new CustomerId() { Id = request.Id }, context);

            return customerGrpc;
        }

        public override async Task<NoParameter> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
            _logger.LogInformation("gRPC request DeleteCustomer has been got");

            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Not found");

            await _customerRepository.DeleteAsync(customer);

            return new NoParameter();
        }
    }
}
